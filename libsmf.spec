Summary: LibSMF
Name: libsmf
Version: 1.3
Release: 1%{?dist}
License: BSD
URL: https://gitlab.com/alextee/libsmf
Source0: https://gitlab.com/alextee/libsmf/-/archive/1.3/libsmf-1.3.tar.gz
Group: Applications/Multimedia
Packager: Alexandros Theodotou

BuildRequires: glib2-devel
BuildRequires: gcc
BuildRequires: pkgconfig
Requires: glibc
Requires: glib2

BuildRoot: ~/rpmbuild/

%description
LibSMF is a BSD-licensed C library for handling SMF ("*.mid") files. It
transparently handles conversions between time and pulses, tempo map handling
etc. The only dependencies are C compiler and glib. Full API documentation and
examples are included.

%package devel
Summary: Development files for %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}

%description devel
The %{name}-devel package contains header files for %{name}.

%prep
%setup -q

%build
%configure
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} install DESTDIR=%{buildroot}

%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root,-)
%doc TODO COPYING NEWS
%{_bindir}/*
%{_mandir}/man1/*
%{_libdir}/*

%files devel
%{_includedir}/*

%changelog
* Thu Jan 17 2019 Alexandros Theodotou <alex at zrythm dot org> 1.3
- RPM release
