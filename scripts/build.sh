#!/bin/sh
cd
curl -O https://gitlab.com/alextee/libsmf/-/archive/$CI_COMMIT_REF_NAME/$_PACKAGE_NAME.tar.gz
tar xvzf $_PACKAGE_NAME.tar.gz
cp $_PACKAGE_NAME/libsmf.spec rpmbuild/SPECS/
cp $_PACKAGE_NAME.tar.gz rpmbuild/SOURCES/
cd rpmbuild/SPECS/
rpmbuild -ba libsmf.spec
